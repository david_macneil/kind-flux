#!/bin/bash
#export KUBECONFIG=~/.kube/kind

# I've used kind instead of minikube, minikube doesn't like my laptop and I haven't
# had the chance to figure out why (driver issues). You can replace this line with whatever
# it takes to get minikube up and running as long as \$KUBECONFIG points at a working
# config file for kubectl to talk to the cluster you're testing on.
#kind create cluster --kubeconfig $KUBECONFIG --config kind-config.yaml

# From here on it should all run ok on minikube (obviously I haven't tested this theory:)
#helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo add fluxcd https://charts.fluxcd.io
helm repo update

kubectl create ns flux
kubectl create -f https://raw.githubusercontent.com/fluxcd/helm-operator/master/deploy/crds.yaml

helm upgrade -i flux fluxcd/flux \
   --set git.url=git@gitlab.com:david_macneil/kind-flux.git \
   --set git.path=workloads \
   --namespace flux \
   --wait

helm upgrade -i helm-operator fluxcd/helm-operator \
   --set git.ssh.secretName=flux-git-deploy \
   --set helm.versions=v3 \
   --namespace flux \
   --wait

kubectl -n flux logs deployment/flux | grep identity.pub | cut -d '"' -f2

read -p "Press return when you've added the ssh pub key (above) to your git repo: "
echo
echo "Creating namespaces..."
for f in ../namespaces/*; do
  kubectl create -f $f
done

## Everything from here on is not required, just waiting on stuff to become ready

echo
echo "This can take >10mins while the containers (drupal and mysql) are being pulled, created and become ready"
echo "You can monitor progress with:"
echo "  KUBECONFIG=${HOME}/.kube/kind kubectl get pods -n drupal -w"
echo
echo "Waiting for drupal pods come up..."
while [[ $(kubectl get pod -n drupal 2>&1 | grep "1/1" -c) -lt 2 ]]; do
  sleep 1
  echo -n "."
done
echo
# Make sure we have the services
echo "Checking services are active..."
while ! kubectl get services -n drupal 2>/dev/null | grep 172; do
  sleep 1
  echo -n "."
done

replica_set=$(kubectl get replicaset -n drupal | tail -n 1 | awk '{print $1}')
kubectl expose -n drupal replicaset $replica_set --type=LoadBalancer
ip_addr=$(kubectl get services -n drupal | grep ^drupal-[a-z0-9]*[[:space:]]*LoadBalancer | awk '{print $4 " " $5}' | sed 's/ [0-9]*:[0-9]*\/TCP,/:/g' | cut -d: -f1-2)

echo "If the IP below is empty then something failed"
echo "IP: $ip_addr"

echo "Checking service is accessible..."
while ! curl -k https://$ip_addr > /dev/null 2>&1; do
  echo -n "."
  sleep 1
done

echo
echo "*** You can now access drupal via https://$ip_addr in your browser"
